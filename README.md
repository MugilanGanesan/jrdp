# JsRDP-Redux
Javascript Remote Desktop Protocol built from scratch by Mugilan Ganesan!

A javascript/python set of programs which act as a remote desktop! Built entirely from scratch with an original protocol powering it. It is custom built to be used as a website, which enables it to be used by literally anybody as long as they have a browser
