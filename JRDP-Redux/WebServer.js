var express = require("express");
var nunjucks = require("nunjucks");
var bodyParser = require("body-parser");

const spawn = require("child_process").spawn;

var process = spawn('python',["DesktopInput.py"]);

var app = express();

app.use(express.urlencoded());

var current_frame = "what"; 
var count = 0

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

nunjucks.configure('views',{
    autoescape: true,
    express: app
});

app.use(express.static('public'));

app.use('/images', express.static(__dirname + '/Images'));

app.get('/', function (req,res){
   res.render('Client.html', {title:"Web Remote Display"})
    
});

app.post('/input', function (req,res){
    res.end();
});

process.stdout.on('data', (data) => {
        try{
            current_frame = JSON.parse(data.toString());
            console.log(count);
            count++;
        }
        catch(err){
            console.log('alright then just ignore this');
        }
});
    
app.get('/render', function (req,res){
    res.set('Content-Type', 'text/plain');
    res.send(current_frame);
    res.end();
    
});
                  
var server = app.listen(80,function(){
    
});