import win32gui, win32ui, win32con, win32api
import mss
from PIL import Image
import ctypes
import ctypes.wintypes
from ctypes.wintypes import HWND, RECT, DWORD
import time
import pybase64
import io
import sys
import zlib
import json

dwm = ctypes.windll.dwmapi

def test():
    ActiveWindowList = WindowProfiler()
    
    PreviousWindowList = ActiveWindowList
    PreviousWindowList.append("lol")
    ActiveWindowList = WindowProfiler()
    
    ListCommands = CompareWindowLists(ActiveWindowList,PreviousWindowList)

    print(ListCommands)
    print(ActiveWindowList)
    
    TopMostWindow = TopMostWindowCreator(ActiveWindowList)

def main():
    PreviousWindowList =  WindowProfiler()
    PreviousTopMostWindow = TopMostWindowCreator(PreviousWindowList)
    while True:
        
        specificCommands = []
        ActiveWindowList = WindowProfiler()
        TopMostWindow = TopMostWindowCreator(ActiveWindowList)
        
        if TopMostWindow['name'] == PreviousTopMostWindow['name']:
            if TopMostWindow['coords'] == PreviousTopMostWindow['coords']:
                if TopMostWindow['data'] == PreviousTopMostWindow['data']:
                    pass
                else:
                    specificCommands.append({'name':TopMostWindow['name'],'data':TopMostWindow['data']})
            else:
                specificCommands.append({'name':TopMostWindow['name'],'coords':TopMostWindow['coords']})
        else:
            specificCommands.append({'name':TopMostWindow['name'],'coords':TopMostWindow['coords'],'data':TopMostWindow['data']})
        
        genericCommands = CompareWindowLists(ActiveWindowList,PreviousWindowList)
        
        PreviousWindowList = ActiveWindowList
        PreviousTopMostWindow = TopMostWindow
        
        print(json.dumps([genericCommands,specificCommands]))
        sys.stdout.flush()
        #print(genericCommands,commands)
        time.sleep(0.032)
        
    
def screenShot(left,top,width,height):
    frame = mss.mss().grab({'top':top,'left':left,'height':height,'width':width})
    img = Image.frombytes("RGB", frame.size, frame.bgra, "raw", "BGRX")
        
    buffered = io.BytesIO()
    img.save(buffered, format="JPEG",quality=50)
    img = str(pybase64.b64encode(buffered.getvalue()))
    img = img[2:]
    img = img[:len(img)-1]
    
    return img

def getPosition(hwnd):
    rect = RECT()
    DWMWA_EXTENDED_FRAME_BOUNDS = 9
    dwm.DwmGetWindowAttribute(HWND(hwnd),DWORD(9), ctypes.byref(rect), ctypes.sizeof(rect))

    return rect.left,rect.top,rect.right-rect.left,rect.bottom-rect.top

def CompareWindowLists(ActiveWindowList,PreviousWindowList):
    WindowCommands = []
    if ActiveWindowList != PreviousWindowList:
        DeletedWindows = list(set(PreviousWindowList) - set(ActiveWindowList))
        for Window in DeletedWindows:
            WindowCommands.append({'del':Window})
            
    return WindowCommands

def objMaker(hwnd, lParam):
    global WindowObject, zOrder
    if win32gui.IsWindowVisible(hwnd) and win32gui.GetWindowText(hwnd) != '':
        if zOrder == 0:
            left,top,width,height = getPosition(hwnd)
            WindowObject = {'name':win32gui.GetWindowText(hwnd),'coords': [left,top,width,height],'data':screenShot(left,top,width,height)}
        zOrder += 1
            
def TopMostWindowCreator(ActiveWindowList):
    global WindowObject, zOrder
    zOrder = 0
    win32gui.EnumWindows(objMaker, None)
    return WindowObject

def windowFiler(hwnd, lParam):
    global windows
    if win32gui.IsWindowVisible(hwnd) and win32gui.GetWindowText(hwnd) != '':
        windows.append(win32gui.GetWindowText(hwnd))
        
def WindowProfiler():
    global windows
    windows = []
    win32gui.EnumWindows(windowFiler, None)
    return windows

main()
